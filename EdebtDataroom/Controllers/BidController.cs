﻿using EdebtDataroom.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using EdebtDataroom.Models;
using EdebtDataroom.DTO;
using EdebtDataroom.Helpers;



namespace EdebtDataroom.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class BidController : ControllerBase
    {
        private DataContext _db;

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Get(int id)
        {
            var rng = new Random();
         //   var bids = await _db.Bids.AllAsync(x => x.Tender_Id.Id == id);
            var bids = _db.Bids.ToList().Where(x => x.Tender_Id.Id == id);
            var userName = HttpContext.User.Identity.Name;
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            return Ok(bids);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBid(int id)
        {
            var userName = HttpContext.User.Identity.Name;
            // var bid =await _db.Bids.FirstOrDefaultAsync(x => x.Id == id);
            var bids = await _db.Bids.Where(x => x.Tender_Id.Id == id).OrderByDescending(x => x.Bid_Value).Take(2).ToListAsync();
            //  var userName= HttpContext.User.Identity.Name;
            return Ok(bids);
        }

        [AllowAnonymous]
        [HttpPost("InsertBid")]
        public async Task<IActionResult> InsertBid(BidInfoDto BidDetails)
        {
           var  BidH = new BidHelpers();
            var userName = HttpContext.User.Identity.Name;
            BidDetails.Username = userName;
            var bid =  BidH.CreateBidObject(BidDetails);
            var tender = await _db.TenderDetails.FirstOrDefaultAsync(x => x.Id == BidDetails.Tender_Id);
            bid.Tender_Id = tender;
            await _db.Bids.AddAsync(bid);
            await _db.SaveChangesAsync();
           
            return Ok(BidDetails);
        }


        public BidController(DataContext db)
        {
            _db = db;
        }
    }
}
