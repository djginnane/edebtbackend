﻿using EdebtDataroom.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EdebtDataroom.Models;

namespace EdebtDataroom.Controllers 
{
    [ApiController]
    [Route("[controller]")]
    public class EdebtUserController : ControllerBase
    {
      

        private DataContext _db;
        [HttpGet]
        public IActionResult Get()
        {
            // var UserList = new List<User>();
         //   var UserList = _db.Users.Where(x => x.User_Type == "u").ToList();
            return Ok("Stuff");
        }

        [HttpGet("getusers")]
        public IActionResult GetUsers()
        {
            //     var UserList = new List<User>();
            var UserList = _db.Users.Where(x => x.User_Type == "u").ToList();
            var User = new User();
            foreach (var u in UserList) { u.PasswordHash = User.PasswordHash; u.PasswordSalt = User.PasswordSalt; }
            return Ok(UserList);
        
        }
        public EdebtUserController(DataContext db)
        {
            _db = db;
        }
    }
}
