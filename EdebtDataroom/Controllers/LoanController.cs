﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EdebtDataroom.Data;
using EdebtDataroom.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace EdebtDataroom.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class LoanController : ControllerBase
    {

        private DataContext _db;

       
        [HttpGet]
        public IActionResult Get()
        {
           
            var loans = _db.TenderDetails.ToList();
            var userName = HttpContext.User.Identity.Name;
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            return Ok(loans);
        }
        [AllowAnonymous]
        [HttpGet("GetLoans")]
        public IActionResult GetLoans2()
        {

            var loans = _db.TenderDetails.ToList();
            return Ok(loans);
        }

        [AllowAnonymous]
        [HttpPost("addloan")]
        public IActionResult AddLoan(TenderDetails loan)
        {
            loan.Date_Added = DateTime.UtcNow;
            _db.Add(loan);
            _db.SaveChanges();
           // var loans = _db.TenderDetails.ToList();
            return Ok(loan);
        }
        public LoanController(DataContext db)
        {
            _db = db;
        }
    }
}
