﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EdebtDataroom.Migrations
{
    public partial class BidsFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bids_TenderDetails_tenderDetailsId",
                table: "Bids");

            migrationBuilder.DropIndex(
                name: "IX_Bids_tenderDetailsId",
                table: "Bids");

            migrationBuilder.DropColumn(
                name: "tenderDetailsId",
                table: "Bids");

            migrationBuilder.RenameColumn(
                name: "Tender_Id",
                table: "Bids",
                newName: "Tender_IdId");

            migrationBuilder.CreateIndex(
                name: "IX_Bids_Tender_IdId",
                table: "Bids",
                column: "Tender_IdId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bids_TenderDetails_Tender_IdId",
                table: "Bids",
                column: "Tender_IdId",
                principalTable: "TenderDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bids_TenderDetails_Tender_IdId",
                table: "Bids");

            migrationBuilder.DropIndex(
                name: "IX_Bids_Tender_IdId",
                table: "Bids");

            migrationBuilder.RenameColumn(
                name: "Tender_IdId",
                table: "Bids",
                newName: "Tender_Id");

            migrationBuilder.AddColumn<int>(
                name: "tenderDetailsId",
                table: "Bids",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Bids_tenderDetailsId",
                table: "Bids",
                column: "tenderDetailsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bids_TenderDetails_tenderDetailsId",
                table: "Bids",
                column: "tenderDetailsId",
                principalTable: "TenderDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
