﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EdebtDataroom.Migrations
{
    public partial class Usrs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "Users",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "Approved_By",
                table: "Users",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Authorised_Person",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Create",
                table: "Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Entity_Name",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Terms_Approved",
                table: "Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "User_Type",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "terms",
                table: "Users",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "UserDoc",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Url = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Doc_Type = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    User_IdId = table.Column<int>(type: "int", nullable: false),
                    UploadDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Uploaded_By = table.Column<int>(type: "int", nullable: false),
                    Document_Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDoc", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserDoc_Users_User_IdId",
                        column: x => x.User_IdId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserDoc_User_IdId",
                table: "UserDoc",
                column: "User_IdId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserDoc");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Approved_By",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Authorised_Person",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Create",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Entity_Name",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Terms_Approved",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "User_Type",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "terms",
                table: "Users");
        }
    }
}
