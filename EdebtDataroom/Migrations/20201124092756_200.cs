﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EdebtDataroom.Migrations
{
    public partial class _200 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProjectName",
                table: "Bids");

            migrationBuilder.AddColumn<DateTime>(
                name: "Bid_Date",
                table: "Bids",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<double>(
                name: "Bid_Value",
                table: "Bids",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "Document_Link",
                table: "Bids",
                type: "varchar(300)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Tender_Id",
                table: "Bids",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Username",
                table: "Bids",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "tenderDetailsId",
                table: "Bids",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TenderDetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LoanName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Reserve = table.Column<double>(type: "float", nullable: false),
                    MaxBid = table.Column<double>(type: "float", nullable: false),
                    BuyPrice = table.Column<double>(type: "float", nullable: false),
                    Username = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Annon_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Annon_Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BidEnd = table.Column<DateTime>(type: "datetime2", nullable: false),
                    BidStart = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Country = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Currency = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ItemValue = table.Column<double>(type: "float", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SellPrice = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Settlement_Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Hide = table.Column<bool>(type: "bit", nullable: false),
                    Unsold = table.Column<int>(type: "int", nullable: false),
                    Contract_Length = table.Column<int>(type: "int", nullable: false),
                    Confirmed = table.Column<int>(type: "int", nullable: false),
                    Ghost_Reserve = table.Column<double>(type: "float", nullable: false),
                    Date_Added = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Date_Modified = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Display_All = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TenderDetails", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Bids_tenderDetailsId",
                table: "Bids",
                column: "tenderDetailsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bids_TenderDetails_tenderDetailsId",
                table: "Bids",
                column: "tenderDetailsId",
                principalTable: "TenderDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bids_TenderDetails_tenderDetailsId",
                table: "Bids");

            migrationBuilder.DropTable(
                name: "TenderDetails");

            migrationBuilder.DropIndex(
                name: "IX_Bids_tenderDetailsId",
                table: "Bids");

            migrationBuilder.DropColumn(
                name: "Bid_Date",
                table: "Bids");

            migrationBuilder.DropColumn(
                name: "Bid_Value",
                table: "Bids");

            migrationBuilder.DropColumn(
                name: "Document_Link",
                table: "Bids");

            migrationBuilder.DropColumn(
                name: "Tender_Id",
                table: "Bids");

            migrationBuilder.DropColumn(
                name: "Username",
                table: "Bids");

            migrationBuilder.DropColumn(
                name: "tenderDetailsId",
                table: "Bids");

            migrationBuilder.AddColumn<string>(
                name: "ProjectName",
                table: "Bids",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
