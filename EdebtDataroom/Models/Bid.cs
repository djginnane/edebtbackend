﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdebtDataroom.Models
{
    public class Bid
    {
        public int Id  { get; set; }
        [Required]
        public TenderDetails Tender_Id { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public double Bid_Value { get; set; }
        [Required]
        public DateTime Bid_Date { get; set; }
        [Column(TypeName = "varchar(300)")]
        public string Document_Link { get; set; }
      
    }
}
