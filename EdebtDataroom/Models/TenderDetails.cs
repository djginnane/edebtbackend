﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EdebtDataroom.Models
{
    public class TenderDetails
    {
        public int Id { get; set; }
        [Required]
        public string LoanName { get; set; }
        public double Reserve { get; set; }
        public double MaxBid { get; set; }
        public double BuyPrice { get; set; }
        [Required]
        public string Username { get; set; }
        public string Annon_Name { get; set; }
        public string Annon_Description { get; set; }
        public DateTime BidEnd { get; set; }
        public DateTime BidStart { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public string Currency { get; set; }
        public double ItemValue { get; set; }
        [Required]
        public string Description { get; set; }
        public string SellPrice { get; set; }
        public DateTime Settlement_Date { get; set; }
        public int Status { get; set; }
        public bool Hide { get; set; }
        public int Unsold { get; set; }
        public int Contract_Length { get; set; }
        public int Confirmed { get; set; }
        public double Ghost_Reserve { get; set; }
        [Required]
        public DateTime Date_Added { get; set; }
        public DateTime Date_Modified { get; set; }
        /** Display this debt to all users, using amnajs teasing approach **/
        public bool Display_All { get; set; }
        public List<Bid> Bids { get; set; }

    }
}
