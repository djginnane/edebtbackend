﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EdebtDataroom.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string Entity_Name { get; set; }
        public DateTime Create { get; set; }
        public int Approved_By { get; set; }
        public ICollection<UserDoc> UserDocuments {get; set;}
        public string User_Type { get; set; }
        public bool Active { get; set; }
        public bool terms { get; set; }
        public DateTime Terms_Approved { get; set; }
        public string Authorised_Person { get; set; }
    }
    public class UserDoc
    {
        public int Id { get; set; }
        [Required]
        public string Url { get; set; }
        public string Doc_Type   { get; set; }
        [Required]
        public User User_Id { get; set; }
        [Required]
        public DateTime UploadDate { get; set; }
        public int Uploaded_By { get; set; }
        public string Document_Description { get; set; }

    }
}
