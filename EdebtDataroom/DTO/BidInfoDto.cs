﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EdebtDataroom.DTO
{
    public class BidInfoDto
    {
     
       
       public int Id { get; set; }
        public int Tender_Id { get; set; }
       
        public string Username { get; set; }
       
        public double Bid_Value { get; set; }
     
      //  public DateTime Bid_Date { get; set; }
       
    }
}
