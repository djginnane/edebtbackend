﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace EdebtDataroom.DTO
{
    public class UserForRegisterDto : EdebtDataroom.Models.User
    {
       
        [Required]
        [StringLength(8, MinimumLength =4, ErrorMessage = "Password must be within at least 4 characters")]
        public string Password { get; set; }
    }
}
