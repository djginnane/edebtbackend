﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EdebtDataroom.DTO;
using EdebtDataroom.Models;
using EdebtDataroom.Repositorys;
using EdebtDataroom.Data;

namespace EdebtDataroom.Helpers
{
    public class BidHelpers
    {
       
        public  Bid CreateBidObject(BidInfoDto bid)
        {
           // var lRepo = new LoanRepository();
            var NewBid = new Bid();
            NewBid.Bid_Date = DateTime.UtcNow;
            NewBid.Bid_Value = bid.Bid_Value;
           // NewBid.Tender_Id = _Lrepo.getLoan(bid.Tender_Id);
            NewBid.Username = bid.Username;
            return NewBid;
        }
        public BidHelpers()
        {
          

        }
    }
}
